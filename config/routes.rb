Rails.application.routes.draw do
  root :to => 'home#index'
  mount ShopifyApp::Engine, at: '/'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post 'prices', to: 'prices#prices'
  post 'update_key', to: 'home#update_key'
end
