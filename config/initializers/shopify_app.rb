ShopifyApp.configure do |config|
  config.application_name = ENV['SHOPIFY_APP_NAME']
  config.api_key = ENV['SHOPIFY_API_KEY']
  config.secret = ENV['SHOPIFY_API_SECRET']
  config.old_secret = ENV['SHOPIFY_API_SECRET']
  config.scope = "write_shipping"
  config.embedded_app = true
  config.after_authenticate_job = false
  config.session_repository = Shop
end
