class PricesController < ApplicationController
  skip_before_action :verify_authenticity_token
  def prices    
    ShopifyHook.create log_string: {params: params.permit!.to_h}

      @nome_loja = params[:rate][:origin][:company_name]+".myshopify.com".downcase
      @nome_loja = @nome_loja.downcase

      key = Shop.where(shopify_domain:"#{@nome_loja}").pluck(:customer_key)

      
      @cliente_chave = key.reduce  
      # cliente_chave = "\"#{@cliente_chave}\"".gsub("\"", "")
      @cep_destino = params[:rate][:destination][:postal_code] rescue nil
      @peso_gramas =  params[:rate][:items].map{|item| (item[:grams] * item[:quantity])}.sum

      
      #binding.pry
      
      url = URI.escape("https://flashcalculador.com.br/api/v2/calcular_frete?cliente_chave=#{@cliente_chave}&cep_destino=#{@cep_destino}&peso_gramas=#{@peso_gramas}")
      
      #binding.pry
      
      hash = RestClient.get(url) 
      shopify_rates = eval(hash)
      
      #binding.pry
      
      rates_products = []
      shopify_rates[:products].each do |p|
        p.each do |key, value|
          product_rate = p.values
          rates_products << {
            service_name: key,
            service_code: 123,#key.parameterize,# (se não funcionar pode deixar 123 pra teste e depois vemos o que fazer)
            total_price: product_rate.reduce[:preco].to_s.delete('.'), # == R$10,00
            currency: 'BRL',
            min_delivery_date: Date.today + product_rate.reduce[:prazo_min].to_i.days,
            max_delivery_date: Date.today + product_rate.reduce[:prazo_max].to_i.days
          }
        end
      end  

      rates = Hash.new 
      rates[:rates] = rates_products    
      render json: rates
       
  end

end
