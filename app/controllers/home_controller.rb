# frozen_string_literal: true

class HomeController < AuthenticatedController
  skip_before_action :verify_authenticity_token
  before_action :set_shop
  
  def index; end
  
  def update_key  
    redirect_params = test_and_save_api_key

    if @shop.carrier_created?
      create_carrier_service

      @shop.update! carrier_created: true
    end
    
    redirect_to root_path(redirect_params)
  end

  private
  def create_carrier_service
    body = JSON.load(Rails.root.join('carrier_service.json'))
    body['carrier_service']['callback_url'] = "#{request.base_url}/prices"

    headers = {content_type: :json, accept: :json}
    headers["X-Shopify-Access-Token"] = "#{shop.shopify_token}"
    
    url = "https://#{shop.shopify_domain}/admin/carrier_services"
    RestClient.post(url, body.to_json, headers)
  end

  def test_and_save_api_key
    begin
      RestClient.get("https://flashcalculador.com.br/api/v2/calcular_frete?cliente_chave=#{params[:api_key]}&cep_destino=00000000&peso_gramas=1000")
      @shop.update customer_key: params[:api_key], active: params[:active].present?
      return {}
    rescue
      return {message: "Chave de API inválida"} 
    end
  end

  def set_shop
    @shop = Shop.find_by_shopify_domain(current_shopify_domain)
  end
end
