class AddCarrierCreatedToShops < ActiveRecord::Migration[5.1]
  def change
    add_column :shops, :carrier_created, :boolean
  end
end
