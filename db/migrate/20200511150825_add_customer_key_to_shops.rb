class AddCustomerKeyToShops < ActiveRecord::Migration[5.1]
  def change
    add_column :shops, :customer_key, :string
  end
end
